from readFile import readingFile
from clean_file_data import cleaningData
from calculation import calculateData

class ExecuteFootball:
    def __init__(self, fileName):
        self.fileName = fileName

    def executeData(self):
        fileVal = readingFile(self.fileName)
        val = fileVal.fileReading()
        clean_data = cleaningData(val,6,8)
        cleaned_data = clean_data.clean_data()
        result = calculateData(cleaned_data,6,8,1)
        final_result = result.calculation()
        return final_result

data=ExecuteFootball("football.dat")
result = data.executeData()
print(result)