import re
def smallest_difference(fileName):
    with open(fileName) as fileReader:
        count=0
        for row in fileReader:
            row_val=row.split()
            if len(row_val)>1 and count>0:
                row_val[6]=re.sub("\D", "",row_val[6] )
                row_val[8]=re.sub("\D", "",row_val[8] )
                temp_diff=abs(int(row_val[6])-int(row_val[8]))
                if count==1:
                    comp_value=temp_diff
                elif temp_diff <comp_value:
                    comp_value=temp_diff
                    team_name=row_val[1]
            count+= 1
    return team_name,comp_value
print(smallest_difference('football.dat'))