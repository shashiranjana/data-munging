from clean_file_data import cleaningData

class calculateData(cleaningData):
    def __init__(self, fileName,firstVal,secondVal,keyVal):
        self.fileName = fileName
        self.firstVal = firstVal
        self.secondVal = secondVal
        self.keyVal = keyVal

    def calculation(self):
        count = 0
        finalValue = {}
        for rowData in self.fileName:
            temp_diff = abs(int(rowData[self.firstVal])-int(rowData[self.secondVal]))
            if count==0:
                comp_value = temp_diff
            elif temp_diff < comp_value:
                comp_value = temp_diff
                index_value = rowData[self.keyVal]
            count+= 1
        finalValue[index_value] = comp_value

        return finalValue
