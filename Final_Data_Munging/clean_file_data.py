from readFile import readingFile
import re

class cleaningData(readingFile):
    def __init__(self, fileName,firstVal,secondVal):
        self.fileName = fileName
        self.firstVal = firstVal
        self.secondVal = secondVal

    def clean_data(self):
        count=0
        cleaned_data=[]
        for rowData in self.fileName:
            if len(rowData)>1 and count>0:
                rowData[self.secondVal] = re.sub(r"\D", "",rowData[self.secondVal] )
                rowData[self.firstVal] = re.sub(r"\D", "",rowData[self.firstVal] )
                cleaned_data.append(rowData)
            count+= 1
        return cleaned_data

