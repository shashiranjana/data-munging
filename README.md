Part 1: Weather Data
File Name:- weather.dat 
You’ll find daily weather data for Morristown, NJ for June 2002. 
Descriptive: Write a program to output the day number (column one) with the smallest temperature spread (the maximum temperature is the second column, the minimum the third column).

Part 2: Soccer League Table
File Name: football.dat
It contains the results from the English Premier League for 2001/2.
Descriptive: The columns labeled ‘F’ and ‘A’ contain the total number of goals scored for and against each team in that season (so Arsenal scored 79 goals against opponents, and had 36 goals scored against them).
Write a program to print the name of the team with the smallest difference in ‘for’ and ‘against’ goals.
