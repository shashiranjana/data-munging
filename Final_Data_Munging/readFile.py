class readingFile:
    def __init__(self, fileName):
        self.fileName = fileName

    def fileReading(self):
        with open(self.fileName) as fileReader:
            listOfFile = []
            for row in fileReader:
                row_val = row.split()
                listOfFile.append(row_val)
        return listOfFile